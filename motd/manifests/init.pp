# Class: motd 
#
# This module manages motd
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class motd {

  file { '/etc/motd':
    source    =>  'puppet:///modules/motd/motd',
    mode      =>  '0444',
    owner     =>  'root',
    group     =>  'root',
  }

}
